<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
/** @var array $data */
$disabled = true;

if($data['audio']){
    $disabled = false;
}
$isConfirmWork = false;
$permission = User::CanConfirmWorkDetail(Yii::$app->user->identity->getId());
$workStatus = [2,3];
if(in_array($data['status'],$workStatus) && $permission){
    $isConfirmWork = true;
}


?>
<div class="row" id="user-work-info">
    <div class="col-md-4" >
        <p>User code: <?= $data['userCode'] ?></p>
        <p>User name: <?= $data['userName'] ?> </p>
        <p>Phone number: <?= $data['phone'] ?> </p>
    </div>
    <div class="col-md-4">
        <p class="info-label">Start time</p>
        <p><?= $data['start']?></p>
        <p class="info-label">Work content</p>
        <p><?= $data['workContent'] ?></p>
    </div>
    <div class="col-md-4">
        <p class="info-label">End time</p>
        <p><?= $data['end'] ?></p>
    </div>
</div>

<div id="work-image">
    <p>Work Detail Image</p>
    <div class="row" id="image">
        <?php foreach ($data['images'] as $image){?>
            <div class="col-md-4 col-image">
                <h4>チェック項目</h4>
                <div class="row">
                    <div class="col-md-7" style="height: 300px">
                        <div class="class_confirm">
                            <i class='fas fa-check' style='color:white'></i>
                            確認内容レコメンド
                        </div>
                    </div>
                    <div class="col-md-5 image-right">
                        <img src=<?= '/basic/upload/'.DIRECTORY_SEPARATOR.$image ?> alt="Image" class="img-responsive" style="height: 300px">

                    </div>
                </div>

            </div>
        <?php }?>
    </div>

    <div class="row">
        <div id="user-comment">
            <p><?= $data['audio']  ?></p>
        </div>
        <div id="confirm-audio-button">
            <?= Html::button(Yii::t('app','Confirm Audio'),['id' => 'confirm-audio','class' => 'btn btn-primary','disabled' => $disabled])?>
        </div>
    </div>
    <div class="row">
        <label><?= Yii::t('app','Confirm comment')?></label>
        <br >
        <input type="text" class="form-control" id="confirm-comment" value="<?= $data['officeComment'] ?>" maxlength="300">
        <div id="button-detail">
            <?= Html::button(Yii::t('app','Decline'),['id' => 'btn-decline','class' => 'btn btn-danger'])?>
            <?= Html::button(Yii::t('app','Accept'),['id' => 'btn-accept','class' => 'btn btn-primary'])?>
        </div>

    </div>
    <div>
        <audio id ="audio-file">
            <?php if($data['audio']) {?>
                <source src=<?= '/basic/upload/'.DIRECTORY_SEPARATOR.$data['audio'] ?> type="audio/mp3">
                Your browser does not support the audio element.
            <?php } ?>
        </audio>
    </div>

</div>

<!--Modal confirm M31-->
<?php
Modal::begin([
    'id' => 'modal-confirm',
    'closeButton' => false,
]);
?>
<div id="confirm-message" style="font-size: 20px; margin-bottom: 15px;">
    <p>確認で送信しますか？</p>
</div>
<div id="btn-group">
    <button class="btn btn-primary" id='btn-confirm'> 確認</button>
    <button class="btn btn-danger" id='btn-not-confirm'>否認</button>
</div>
<?php Modal::end(); ?>

<!--Modal decline M30-->
<?php
Modal::begin([
    'id' => 'modal-decline',
    'closeButton' => false,
]);
?>
<div id="confirm-message" style="font-size: 20px; margin-bottom: 15px;">
    <p>否認で送信しますか？</p>
</div>
<div id="btn-group">
    <button class="btn btn-primary" id='btn-yes'> 確認</button>
    <button class="btn btn-danger" id='btn-no'>否認</button>
</div>
<?php Modal::end(); ?>

<!--modal image-->
<?php
Modal::begin([
    'id' => 'modal-image',
    'closeButton' => false,
    'size' => 'modal-dialog'
]);
?>
<img class="image_modal" src="" alt="" style="width: 100%;height: 100%">
<?php Modal::end(); ?>
<!--end modal image-->


<script>
    var csrf = "<?=Yii::$app->request->getCsrfToken()?>";
    var urlUpdateStatusWork = "<?= \yii\helpers\Url::to(['work/update-status','id' => $_GET['id']])?>";
    var isConfirmWorkDetail = "<?php echo $isConfirmWork ?>" ;
</script>



<?php $this->registerCssFile('@web/css/work-detail.css') ?>
<?php $this->registerJsFile('@web/js/work-detail.js',['depends' => [\yii\web\JqueryAsset::className()]]) ?>




