<?php
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;

$identify = [
    0 => 'Short place',
    1 => 'High place',
];

?>

<div>
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-md-6">
            <button class="btn btn-default btn-event" id="btn-filter"><?= Yii::t('app', 'Filter')?></button>
            <button class="btn btn-default btn-event" id="btn-clear"><?= Yii::t('app', 'Clear')?></button>
        </div>
    </div>
    <table class="table table-bordered" style="text-align: center;">
        <thead>
        <tr class="header-table">
            <th rowspan="2">DL</th>
            <th rowspan="2">WorkId</th>
            <th rowspan="2">Finish time</th>
            <th rowspan="2">Operation Kind</th>
            <th rowspan="2">Identify the high place</th>
            <th colspan="4">Organization</th>
            <th rowspan="2">Seri ID</th>
            <th rowspan="2">Date</th>
            <th rowspan="2">Work Content</th>
            <th colspan="4">Picture</th>
            <th rowspan="2">Audio</th>
            <th rowspan="2">KY division</th>
            <th rowspan="2">Result</th>
        </tr>
        <tr class="header-table">
            <th>Branch/Office</th>
            <th>FSC/Facility</th>
            <th>Employee</th>
            <th>Working employee</th>
            <th>Picture 1</th>
            <th>Picture 2</th>
            <th>Picture 3</th>
            <th>#</th>
        </tr>
        </thead>
        <tbody>
            <?php
            /** @var array $dataWorkList */
            foreach ($dataWorkList as $row){

            ?>
                <tr>
                    <td><input type="checkbox" class="cb-download" name="cb-download-<?= $row['id'] ?>" value="<?= $row['id'] ?>"></td>
                    <td><?= $row['id'] ?></td>
                    <td><?= $row['end'] ?></td>
                    <td><?= $row['faOperationKind'] ?></td>
                    <td>
                       <?php
                        $value = '';
                        if($data = $row['officeArialWork'] || $data = $row['selfArialWork']){
                            $value = $data == 1 ? 'High place': 'Short place';
                        }
                        echo $value;
                        ?>
                    </td>
                    <td><?= $row['level_name 2'].'-'.$row['level_name 3'] ?></td>
                    <td><?= $row['level_name 4'] ?></td>
                    <td></td>
                    <td><?= $row['userName'] ?></td>
                    <td><a href="<?= Url::to(['work/work-detail','id' => $row['id']])?>"><?= $row['faOperationKind'].'-'.$row['start'].'-'.$row['id'] ?></a></td>
                    <td><?php
                        $value = $row['date'];
                        if($row['start']){
                            $value .= '-'.$row['start'];
                        }
                        echo $value;
                        ?>
                    </td>
                    <td><?php
                        $value = '';
                        if ($row['nameConfirmType']) {
                            $value = $row['nameConfirmType'];
                        }
                        if ($row['nameWorkType']) {
                            $value = $row['nameWorkType'];
                        }
                        echo $value;
                        ?>
                    </td>

                        <?php
                        if (isset($row['images'])){
                            $count = 0;
                            foreach ($row['images'] as $key => $image){
                                $count++;
                               if ($key == 3){
                                   break;
                               }
                        ?>
                                <td>
                                    <img src=<?= '/basic/upload/'.DIRECTORY_SEPARATOR.$image ?> alt="Image" class="img-responsive" style="height: 80px">
                                </td>
                            <?php }
                            if($count < 3)
                            {
                                for($i = $count ; $i < 3 ; $i++)
                                {
                                    ?>
                                    <td>
                                    </td>
                                    <?php
                                }
                            }
                            ?>
                        <?php } else {
                            echo '<td></td><td></td><td></td>';
                        } ?>

                    <td>
                        <?php
                            if(is_array(isset($row['images'])) && count(isset($row['images']) > 3)){
                                echo '有';
                            }else{
                                echo '無';
                            }
                        ?>
                    </td>
                    <!-- $row['audioName']-->
                    <td style="color:lightskyblue;text-decoration: underline;" id="td-audio">
                        <?php if($row['audioName']) {?>
                        <p id ="confirm-audio" style="cursor: pointer;">Play</p>
                        <audio id ="audio-file">
                                <source src=<?= '/basic/upload/'.DIRECTORY_SEPARATOR.$row['audioName'] ?> type="audio/mp3">
                                Your browser does not support the audio element.
                        </audio>
                        <?php } ?>
                    </td>


                    <td></td>
                    <td>
                        <?php
                            $value = '';
                            switch ($row['status']){
                                case 4:
                                    if($row['officeComment']){
                                        $value = 'コメント有り';
                                    }else{
                                        $value = '確認済';
                                    }
                                    break;
                                case 5:
                                    $value = '不備有り';
                                    break;
                            }
                            echo $value;
                        ?>
                    </td>
                </tr>


            <?php
            }
            ?>

        </tbody>
    </table>
</div>
<!--Filter modal-->
<?php
Modal::begin([
    'header' =>  Yii::t('app','Filter'),
    'id' => 'filter',
    'closeButton' => false,
    'size' => 'modal-lg'
]);
?>
<?php $form = ActiveForm::begin(
    [
        'id' => 'filter-search-form',
        'fieldConfig' => [
            'enableError' => false,
        ],
    ]
)
?>
<div style="text-align: center">
    <div class="form-inline">
        <div class="form-group filter-form" >
            <?= $form->field($model,'startDate')->Widget(
                DatePicker::className(),
                [
                    'name' => 'startDate',
                    'value' => Date('Y-m-d'),
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
//                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]
            )->label(Yii::t('app','Date'))?>
        </div>
        <div class="form-group filter-form">
            <?= $form->field($model,'endDate')->widget(
                DatePicker::className(),
                ['name' => 'endDate',
                    'value' => Date('Y-m-d'),
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
//                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]
            )->label('~')?>
        </div>
    </div>

    <div class="form-inline">
        <div class="form-group filter-form">
            <?= $form->field($model,'startFinishTime')->widget(
                DatePicker::className(),
                [
                    'name' => 'startFinishTime',
                    'value' => Date('Y-m-d'),
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
//                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]
            )->label(Yii::t('app','Finish time'))?>
        </div>
        <div class="form-group filter-form">
            <?= $form->field($model,'endFinishTime')->widget(
                DatePicker::className(),
                [
                    'name' => 'endFinishTime',
                    'value' => Date('Y-m-d'),
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
//                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]
            )->label('~')?>
        </div>
    </div>

    <div class="form-inline">
        <div class="form-group filter-form" >
            <?= $form->field($model,'projectId')->label(Yii::t('app','Project Id'))->input('text',['class' =>'form-control','id' => 'projectId'])?>
        </div>
        <div class="form-group filter-form">
            <?= $form->field($model,'workType')->dropDownList(
            /** @var array $workType */
                $workType,
                ['prompt'=>'Select Option']
            )->label(Yii::t('app','Work type'))?>
        </div>
    </div>

    <div class="form-inline">
        <div class="form-group filter-form">
            <?= $form->field($model,'identifyPlace')->dropDownList(
                $identify,
                ['prompt'=>'Select Option']
            )->label(Yii::t('app','Identify place'))?>
        </div>


        <div class="form-group filter-form">
            <label class="control-label" for="workType"><?= Yii::t('app','Branch')?></label>
            <select class="form-control" name="" id="branch">
                <option value="">Select Option</option>
                <?php
                     foreach ($dataWorkList as $rows) {
                ?>
                    <option value=""><?= $rows['level_name 2'] ?></option>
                <?php } ?>
            </select>
        </div>
       <!-- <div class="form-group filter-form">
            <?php /* $test = $row['level_name 2']  */?>
            <?/*= $form->field($model,'level_name_2')->dropDownList(
                $row['level_name 2'],
                ['prompt'=>'Select Option']
            )->label(Yii::t('app','Branch'))*/?>
        </div>-->

    </div>


</div>

<?php ActiveForm::end() ?>
<?php Modal::end(); ?>



<?php $this->registerCssFile('@web/css/work-list.css') ?>
<?php $this->registerJsFile('@web/js/work-list.js',['depends' => [\yii\web\JqueryAsset::className()]]) ?>