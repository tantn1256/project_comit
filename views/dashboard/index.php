<?php
/** @var yii\web\View\ $this */
use yii\web\View;
$this->title = Yii::t('app','Confirm Work');
?>
<div class="container-fluid" style="width:80%; margin-top: 30px">
    <div class="header">
        <div id="first-item"><?= Yii::t('app','Company')?></div>
        <div class="item-value"> <?= Yii::t('app','Wait for confirm')?></div>
        <div class="item-value"> <?= Yii::t('app','Working')?></div>
        <div class="item-value"> <?= Yii::t('app','Register')?></div>
    </div>
    <div class="items">
        <?= $data ;?>
    </div>

</div>


<?php $this->registerCssFile('@web/css/table.css')?>

