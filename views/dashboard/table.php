<?php
use yii\helpers\Url;
/** @var id $organizationId */
?>

<?php



if (isset($data['waitting'])) {
    foreach ($data['waitting'] as $row) {

        $url = Url::to(['work/work-detail','id' => $row['id']]);
        ?>
        <tr style="background-color: deeppink" class="row-waitting" onclick="window.location.href = '<?= $url ?>'">
            <td> <?= $row['level_name 2'] ?> </td>
            <td> <?= $row['level_name 3'] ?> </td>
            <td> <?= $row['level_name 4'] ?> </td>
            <td> <?= $row['level_name 5'] ?> </td>
            <td> <?= $row['userName'] ?> </td>
            <td> <?= $row['faOperationKind'] ?> </td>
            <td> <?= $row['nameWorkType'] ?> </td>
            <?php
            date_default_timezone_set("Asia/Bangkok");
            $start = strtotime($row['start']);
            $timestamp = time();
            $hours_tinh =  $timestamp - $start;
            $hours = floor($hours_tinh / 3600);
            $minutes = floor(($hours_tinh / 60) % 60);
            ?>
            <td> <?= date('H:i',$start) ?> </td>
            <?php
                if ($start < 0){
                ?>
                    <td></td>
            <?php }else{ ?>
                    <td><?= "$hours:$minutes"?></td>
            <?php } ?>
            <td> <?= $row['end'] ?> </td>
        </tr>
        <?php
    }
}

if (isset($data['working'])) {
    foreach ($data['working'] as $row) {
        $url = Url::to(['work/work-detail','id' => $row['id']]);
        ?>
        <tr style="background-color: #0a73bb" class="row-waitting" onclick="window.location.href = '<?= $url ?>'">
            <td> <?= $row['level_name 2'] ?> </td>
            <td> <?= $row['level_name 3'] ?> </td>
            <td> <?= $row['level_name 4'] ?> </td>
            <td> <?= $row['level_name 5'] ?> </td>
            <td> <?= $row['userName'] ?> </td>
            <td> <?= $row['faOperationKind'] ?> </td>
            <td> <?= $row['nameWorkType'] ?> </td>
            <?php
            date_default_timezone_set("Asia/Bangkok");
            $start = strtotime($row['start']);
            $timestamp = time();
            $hours_tinh =  $timestamp - $start;
            $hours = floor($hours_tinh / 3600);
            $minutes = floor(($hours_tinh / 60) % 60);
            ?>
            <td> <?= date('H:i',$start) ?> </td>
            <?php
            if ($start < 0){
                ?>
                <td></td>
            <?php }else{ ?>
                <td><?= "$hours:$minutes"?></td>
            <?php } ?>
            <td> <?= $row['end'] ?> </td>
        </tr>
        <?php
    }
}

if (isset($data['decline'])) {
    foreach ($data['decline'] as $row) {
        $url = Url::to(['work/work-detail','id' => $row['id']]);
        ?>
        <tr class="row-waitting" onclick="window.location.href = '<?= $url ?>'">
            <td> <?= $row['level_name 2'] ?> </td>
            <td> <?= $row['level_name 3'] ?> </td>
            <td> <?= $row['level_name 4'] ?> </td>
            <td> <?= $row['level_name 5'] ?> </td>
            <td> <?= $row['userName'] ?> </td>
            <td> <?= $row['faOperationKind'] ?> </td>
            <td> <?= $row['nameWorkType'] ?> </td>
            <?php
            date_default_timezone_set("Asia/Bangkok");
            $start = strtotime($row['start']);
            $timestamp = time();
            $hours_tinh =  $timestamp - $start;
            $hours = floor($hours_tinh / 3600);
            $minutes = floor(($hours_tinh / 60) % 60);
            ?>
            <td> <?= date('H:i',$start) ?> </td>
            <?php
            if ($start < 0){
                ?>
                <td></td>
            <?php }else{ ?>
                <td><?= "$hours:$minutes"?></td>
            <?php } ?>
            <td> <?= $row['end'] ?> </td>
        </tr>
        <?php
    }
}

if (isset($data['finish'])) {
    foreach ($data['finish'] as $row) {
        $url = Url::to(['work/work-detail','id' => $row['id']]);
        ?>
        <tr style="background-color: grey" class="row-waitting" onclick="window.location.href = '<?= $url ?>'">
            <td> <?= $row['level_name 2'] ?> </td>
            <td> <?= $row['level_name 3'] ?> </td>
            <td> <?= $row['level_name 4'] ?> </td>
            <td> <?= $row['level_name 5'] ?> </td>
            <td> <?= $row['userName'] ?> </td>
            <td> <?= $row['faOperationKind'] ?> </td>
            <td> <?= $row['nameWorkType'] ?> </td>
            <?php
            date_default_timezone_set("Asia/Bangkok");
            $start = strtotime($row['start']);
            $timestamp = time();
            $hours_tinh =  $timestamp - $start;
            $hours = floor($hours_tinh / 3600);
            $minutes = floor(($hours_tinh / 60) % 60);
            ?>
            <td> <?= date('H:i',$start) ?> </td>
            <?php
            if ($start < 0){
                ?>
                <td></td>
            <?php }else{ ?>
                <td><?= "$hours:$minutes"?></td>
            <?php } ?>
            <td> <?= $row['end'] ?> </td>
        </tr>
        <?php
    }
}

?>



