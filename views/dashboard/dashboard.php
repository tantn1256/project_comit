<?php
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\components\MyHelpers;
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var  array $information */
/** @var int $id */

?>
<div class="info-filter">
    <div id="organization-info">
        <div class="info">
            <div class="info-name"><?= Yii::t('app','Register')?></div>
            <div class="value">
                <?=
                /** @var array $organization */
                $organization['numberOfUsers'];
                ?>
            </div>
        </div>
        <div class="info">
            <div class="info-name"><?= Yii::t('app','Working')?></div>
            <div class="value">
                <?=
                /** @var array $categories */
                MyHelpers::getWorkingTotal($categories,$organization['id'])
                ?>
            </div>
        </div>
        <div class="info">
            <div class="info-name"><?= Yii::t('app','Wait for confirm')?></div>
            <div class="value">
                <?=
                /** @var array $categories */
                MyHelpers::getWaitListTotal($categories,$organization['id']);
                ?>
            </div>
        </div>
    </div>
    <div id="organization-filter">
        <label class="checkbox-inline"><input type="checkbox" id="cb-working" class="checkbox-group" name="cb-working"><?= Yii::t('app','Working')?></label>
        <label class="checkbox-inline"><input type="checkbox" id="cb-confirm" class="checkbox-group" name="cb-confirm"><?= Yii::t('app','Waiting confirm')?></label>
        <label class="checkbox-inline"><input type="checkbox" id="cb-decline" class="checkbox-group" name="cb-decline"><?= Yii::t('app','Decline')?></label>
        <label class="checkbox-inline"><input type="checkbox" id="cb-finished" class="checkbox-group" name="cb-finished"><?= Yii::t('app','Finished')?></label>
        <a href="" onClick="document.location.reload(true)">
            <span class="glyphicon glyphicon-refresh pull-right" style="font-size: 30px; margin-right: 5px;"></span>
        </a>
    </div>
</div>
<table class="table table-bordered">
    <thead>
    <tr>
        <td>支店</td>
        <td>営業所</td>
        <td>FSC</td>
        <td>拠点</td>
        <td>社員名</td>
        <td>案件種別</td>
        <td>作業内容</td>
        <td>開始</td>
        <td>経過時間</td>
        <td>完了時刻</td>
    </tr>
    </thead>
    <tbody id="tb_dashboard_t">
        <?php
            echo $this->render('table',['data' => $data,'organization' => $organization,'categories'=>$categories]);
        ?>
    </tbody>
</table>




<script>
    var urlEditCheckbox = "<?= \yii\helpers\Url::to(['dashboard/dashboard'])?>";
    var organizationId = "<?= $organization['id'] ?>";
    function autoRefreshPage()
    {
        window.location = window.location.href;
    }
    setInterval('autoRefreshPage()', 180000);


</script>
<?php $this->registerCssFile('@web/css/dash-board.css')?>
<?php $this->registerJsFile('@web/js/dash-board.js',['depends' => [\yii\web\JqueryAsset::className()]]);?>