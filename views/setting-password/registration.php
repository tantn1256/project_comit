<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div id="expired-registration-content">
    <p><?= Yii::t('app','パスワード登録用リンクの有効期限が切れています。')?></p>
    <p><?= Yii::t('app','有効期限は1時間です。')?></p>
    <p><?= Yii::t('app','お手数ですが、ログイン画面から再度パスワード再発行を行なってください。')?></p>
    <div id="btn-group" style="margin:80px auto">
        <a href="<?= Url::to(['site/login'])?>" class="btn btn-info" style="width: 160px;"><?= Yii::t('app','ログイン画面へ')?></a>
    </div>

</div>

<?php $this->registerCssFile('@web/css/login.css')?>
