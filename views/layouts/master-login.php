<?php

use app\assets\AppAsset;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>

</head>
<body>
<style>
    a.pull-right {
        color: white;
    }
    a.center {
        color: white;
        text-decoration: underline;
        font-size: 20px;
        margin-left: 5%;
    }
</style>
<?php $this->beginBody() ?>

<div class="wrap">
    <div>
        <h3 id="header-title">
            初期表示
            <?php

            if(!Yii::$app->user->isGuest) {
                $user = Yii::$app->user->identity;
                $levelOranUser = \app\models\Organization::findOne(['id' => $user['organizationId']]);
                if($levelOranUser['organizationLevelId'] == 4 || $levelOranUser['organizationLevelId'] == 5) {
                    $url = \yii\helpers\Url::to(['dashboard/dashboard','organizationId' => $levelOranUser->id]);
                } else {
                    $url = \yii\helpers\Url::to(['dashboard/index','organizationId' => $levelOranUser->id]);
                }

                ?>
                    <a class="center" href="<?php echo $url ?>">Top</a>
                <?php
            }
            ?>




            <?= Html::a('Work List',['work/work-list'], ['class'=> 'center']) ?>
            <?= Html::a('Manage',[''], ['class'=> 'center']) ?>
            <?= Html::a('ログアウト', ['/site/logout'], ['class'=> 'pull-right']) ?>



        </h3>
    </div>
    <div class="container-fluid">
        <?= $content ?>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>