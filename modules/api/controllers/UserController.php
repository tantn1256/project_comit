<?php


namespace app\modules\api\controllers;

use app\models\User;
use app\models\Work;
use Yii;
use yii\rest\ActiveController;
use app\modules\api\controllers\BasicController;



class UserController extends BasicController
{
    public function actionWorks($userCode){
        /** @var User $user */
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user = User::findByUserCode($userCode);
        if($user){
            Yii::$app->response->statusCode = 200;
            $worksByUser = Work::find()
                ->select(['id','code','date','officeComment','status','faOperationId'])
                ->where(['userCode' => $userCode,'date' => Date('Y-m-d')])
                ->all();

            $response = [
                'data' => $worksByUser,
            ];
        }else{
            Yii::$app->response->statusCode = 400;
            $response = [
                'errors' => [
                    "userCode" =>  "User code not exist.",
                ],
            ];
        }

        return $response;
    }


    /**
     * @param $userCode
     * @return array
     */
    public function actionCreateWork($userCode){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($userCode){
            $requestData = Yii::$app->request->post();
            /** @var User $user */
            $user = User::findByUserCode($userCode);
            if($user){
                $requestData['date'] = Date('Y-m-d');
                $requestData['status'] = 1;
                $requestData['userCode'] = $userCode;
                $requestData['workedOrganizationId'] = $user->organizationId;
                $checkNd = Work::find()->where(['userCode'=>$userCode,'date'=>$requestData['date'],'code'=>$requestData['code']])->orderBy(['id'=>SORT_DESC])->one();
                if ($checkNd){
                    return $response = [
                        'errors' => [
                            "code" =>  "code invalid",
                        ],
                    ];
                }

                $work = new Work();
                $work->attributes = $requestData;
                if($work->save()){
                    Yii::$app->response->statusCode = 200;
                    $response = [
                        'id' => $work->id,
                        'code' => $work->code,
                        'date' => $work->date,
                        'status' => $work->status,
                    ];
                }else{
                    Yii::$app->response->statusCode = 400;
                    $response = [
                        'errors' => [
                            "code" =>  "Data input invalid",
                        ],
                    ];
                }
            }else{
                Yii::$app->response->statusCode = 400;
                $response = [
                    'errors' => [
                        'userCode' => 'User code not exist.',
                    ],
                ];
            }
        }else{
            Yii::$app->response->statusCode = 400;
            $response = [
                'errors' => [
                    'userCode' => 'User code not exist.',
                    "code" =>  "Data input invalid",
                ],
            ];
        }
        return $response;

    }



}