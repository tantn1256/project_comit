<?php


namespace app\modules\api\controllers;


use app\Constant\Constant;
use app\models\Organization;
use app\models\User;
use Yii;
use yii\helpers\Json;
use yii\web\Controller;

class AuthController extends Controller
{
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->enableCsrfValidation = false;
    }

    public function actionIndex(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $userCode = Yii::$app->request->post('code','');
        $password = Yii::$app->request->post('password','');

        $user = User::find()->where(['userCode' => $userCode])->one();
        /** @var User $user */
        if($user){
            if($user->isLocked != Constant::IS_LOCKED){
                /** @var Organization $organization */
                $organization = Organization::findOne($user->organizationId);

                if($organization){

                    if($organization->organizationLevelId == Constant::FIFTH_LEVEL_ID && $user->validatePassword($password)){
                        Yii::$app->response->statusCode = 200;

                        //reset isLocked and attemptCount
                        $attributes = [
                            'isLocked' => Constant::UN_LOCKED,
                            'attemptCount' => Constant::MIN_ATTEMPT_COUNT
                        ];
                        $user->updateAttributes($attributes);

                        $listOrganizationId = [
                            [
                                'id' => $user->organizationId,
                                'organizationLevelId' => $organization->organizationLevelId
                            ]
                        ];

                        for($i=0;$i<4;$i++){
                            $organization = Organization::findOne($organization->parentId);
                            $listOrganizationId[] = [
                                'id' => $organization->id ,
                                'organizationLevelId' => $organization->organizationLevelId
                            ];
                        }

                        $response = [
                            'id' => $user->id,
                            'kyotenType' => $user->kyotenType,
                            'listOrganizationId' => $listOrganizationId,
                        ];

                    }else{
                        Yii::$app->response->statusCode = 401;
                        $user->attemptCount = $user->attemptCount+1;
                        if($user->attemptCount == Constant::MAX_ATTEMPT_COUNT){
                            $user->isLocked = Constant::IS_LOCKED;
                        }
                        $user->update();
                        $response = [
                            'errors' => [
                                'code' => 'The Usercode or the password is incorrect',
                            ],
                        ];
                    }
                }else{
                    Yii::$app->response->statusCode = 500;
                    $response = [
                        'error' => 'loi',
                    ];
                }
            }else{
                Yii::$app->response->statusCode = 401;
                $response = [
                    'errors' => [
                        'message' => 'Account locked !!',
                    ],
                ];
            }

        }else{
            Yii::$app->response->statusCode = 401;
            $response = [
                'errors' => [
                    'code' => "Usercode invalid"
                ],
            ];
        }
        return $response;
    }

   /* public function actionLogout()
    {
        $userID = Yii::$app->request->post('code','');

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $userModel = User::find()->where(['userCode'=>$userID])->one();
        if($userModel){
            Yii::$app->response->statusCode = 200;
            $response = [
                    'message' => 'Logout success',
            ];
        }
        else{
            Yii::$app->response->statusCode = 401;
            $response = [
                'errors' => [
                    'code' => "User code invalid."
                ],
            ];
        }
        return $response;
    }*/
}