<?php


namespace app\modules\api\controllers;

use app\Constant\Constant;
use app\modules\api\controllers\BasicController;
use foo\bar;
use Yii;
use app\models\AlertRecommend;
use app\models\MasterUpdate;

class RecommendController extends BasicController
{
    public function actionRecommend($time){
        if(preg_match('/^[0-9]*$/',$time) && strlen($time) == Constant::NUMBER_TEN){
            $latestUpdate = MasterUpdate::findOne(['key' =>'recommend']);
            if($latestUpdate){
                Yii::$app->response->statusCode = 200;
                $latestTime = strtotime($latestUpdate->updateTime);
                if($latestTime > intval($time)){
                    $recommends = AlertRecommend::find()->asArray()->all();
                    $response = [
                        'data' => $recommends
                    ];
                }else{
                    $response = ['data' => []];
                }
            }else{
                Yii::$app->response->statusCode = 500;
                $response = [
                    'error' => 'An error occurred',
                ];
            }

        }else{
            Yii::$app->response->statusCode = 401;
            $response = [
                    'errors' => [
                        'time' => 'Time format invalid.'
                ]
            ];
        }

        return $response;
    }
}