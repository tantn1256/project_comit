<?php


namespace app\modules\api\controllers;

use app\Constant\Constant;
use app\models\Audio;
use app\models\Image;
use app\models\Log;
use app\models\OfficeCheckTask;
use app\models\SelfCheckTask;
use app\models\User;
use app\models\Work;
use Yii;
use yii\db\Exception;
use app\modules\api\controllers\BasicController;



class WorkController extends BasicController
{
    /**
     * @return array
     */
    public function actionFinishWork(){
        $transaction = Yii::$app->db->beginTransaction();
        $userCode = Yii::$app->request->getBodyParam('userCode');
        $userLogin = Yii::$app->user->identity->userCode;
        if ($userCode == $userLogin){
            $data = Yii::$app->request->getBodyParam('data');
            /** @var User $user */
            $user = User::findByUserCode($userCode);
            if($user  && is_array($data)){
                $errors = [];
                try{
                    foreach($data as $row){
                        $isLogSave = true;
                        $workId = $row['workId'];
                        /** @var Work $work */
                        $work = Work::findOne($workId);
                        if($work){
                            $logs = is_array($row['logs']) ? $row['logs'] : '';
                            if($logs){
                                foreach ($logs as $log){
                                    if ($work->status != 6){
                                        if ($log['flow'] =='' || $log['workId'] =='' || $log['workDate'] =='' || $log['eventId'] ==''  ){
                                            $errors = ['message' => 'this field cannot be empty'];
                                            throw new Exception('this field cannot be empty');
                                        }else{
                                            /** @var Log $newLog */
                                            $newLog = new Log;
                                            $newLog->workId = $workId;
                                            $newLog->flow = $log['flow'];
                                            $newLog->eventId = $log['eventId'];
                                            $newLog->workTypeId = $log['workTypeId'];
                                            $newLog->workDate = date('Y-m-d',intval($log['workDate'])/1000);
                                            $newLog->status = $log['status'] == true ? 1:0;
                                            $newLog->time = date('Y-m-d');
                                            $newLog->datetime = date('Y-m-d H:i:s');
                                            $newLog->userId = $user->id;
                                            $isLogSave = $newLog->save();
                                        }
                                    }else{
                                        $errors =['message' => 'Errors WorkID invalid !!!'];
                                        throw new Exception('Errors WorkID invalid !!!',$errors);
                                    }
                                }
                            }else{
                                $errors =['logs' => 'Logs invalid'];
                                throw new Exception('Logs invalid',$errors);
                            }

                            if($isLogSave){
                                $end = isset($row['end']) ? $row['end'] : '';

                                if($end){
                                    $work->end = date('Y-m-d H:i:s',intval($end)/1000);
                                }
                                $work->status = 6;
                                if($work->save()){
                                    Yii::$app->response->statusCode = 200;
                                    $response['response'][] = [
                                        'workId' => $workId,
                                        'status' => true
                                    ];
                                }else{
                                    $errors = $work->errors;
                                    throw new Exception('Updating work failed');
                                }
                            }
                        }else{

                            $errors = ['workId' => 'workId invalid'];
                            throw new Exception('WorkId invalid');
                        }
                    }
                    $transaction->commit();
                }catch(\Exception $e){
                    Yii::$app->response->statusCode = 400;
                    $transaction->rollBack();
                    $response = [
                        'errors' =>  $errors
                    ];
                }

            }else{
                Yii::$app->response->statusCode = 400;
                $response = [
                    'errors' => 'User code or data invalid'
                ];
            }
        } else{
            Yii::$app->response->statusCode = 500;
            $response = [
                'userCode' => 'Value of userCode invalid'
            ];
        }
        return $response;
    }

    public function actionPushWork($workId){
        $transaction = Yii::$app->db->beginTransaction();
        $requestData = Yii::$app->request->getBodyParams();
        $confirmTypeId = Yii::$app->request->getBodyParam('confirmTypeId');
        $workTypeId = Yii::$app->request->getBodyParam('workTypeId');
        if($workId && (isset($confirmTypeId) || isset($workTypeId))){
            try{
                /** @var Work $work */
                $work = Work::findOne($workId);
                if ($work){
                    /** @var OfficeCheckTask $task */
                    $status = Constant::OFFICE_CHECK_TASK_STATUS;
                    $errors = [];
                    if ($workTypeId){
                        //self check task
                        $task = new SelfCheckTask();
                        $task->workTypeId = $workTypeId;
                        $task->isAutomatic = isset($requestData['isAutomatic']) ? $requestData['isAutomatic'] : '';
                        $status = Constant::SELF_CHECK_TASK_STATUS;
                    }
                    if ($confirmTypeId){
                        //office check task
                        $task = new OfficeCheckTask();
                        $task->confirmTypeId = $confirmTypeId;
                        $task->comment = isset($requestData['comment']) ? $requestData['comment'] : '';
                    }
                    $task->userCode = $work->userCode;
                    $task->workId = $workId;
                    $task->state = 1;
                    if ($task->save()){
                        //save audio, image, checkType
                        $saveImageSuccess = $saveAudioSuccess = true;
                        $audioFileName = Yii::$app->request->getBodyParam('audioFileName');
                        $images = Yii::$app->request->getBodyParam('images');
                        $checkType = Yii::$app->request->getBodyParam('checkType');
                        if($audioFileName){
                            $audio = new Audio;
                            $audio->checkType= $checkType;
                            $audio->taskId= $task->id;
                            $audio->fileCode= $audioFileName;
                            $audio->fileName= $audioFileName;
                            $saveAudioSuccess = $audio->save();
                            if(!$saveAudioSuccess){
                                $errors = $audio->errors;
                                $transaction->rollBack();
                                //throw Exception audio error
                                throw new Exception('Saving audio is failed');
                            }
                        }

                        if(is_array($images)){
                            foreach($images as $imageData ){
                                $fileName = isset($imageData['fileName']) ? $imageData['fileName'] : '';
                                $image = new Image;
                                $image->checkType = $checkType;
                                $image->taskId = $task->id;
                                $image->fileCode = $fileName;
                                $image->fileName = $fileName;
                                $image->objectRecognizeResult = isset($imageData['isObjectRecognizeSuccess']) ? $imageData['isObjectRecognizeSuccess']:'';
                                $image->checkItemId = isset($imageData['checkItemId']) ? $imageData['checkItemId']:'';
                                $image->isConfirmed = isset($imageData['isConfirmed']) ? $imageData['isConfirmed']:'';;
                                $saveImageSuccess= $image->save();
                                if(!$saveImageSuccess){
                                    $errors = $image->errors;
                                    $transaction->rollBack();
                                    //throw Exception images error
                                    throw new Exception('Saving images is failed');
                                    break;
                                }
                            }
                        }

                        if($saveAudioSuccess && $saveImageSuccess){
                            //update status in work table
                            $work->status = $status;
                            $work->powerAIImage = Yii::$app->request->getBodyParam('powerAIImage');
                            if ($workTypeId){
                                $work->start = Date('Y-m-d H:i:s');
                            }
                            if($work->save()){

                                Yii::$app->response->statusCode = 200;
                                $response = $work;
                            }else{
                                $errors = $work->errors;
                                throw new Exception('Saving work is failed');
                            }
                        }

                    }//Validate fail task
                    else{
                        $transaction->rollBack();
                        $errors = $task->errors;
                        //throw Exception task error
                        throw new Exception('Saving task is failed');
                    }
                }else{
                    $errors = ['workId' => 'workId invalid'];
                    throw new Exception('WorkId invalid');
                }
                $transaction->commit();
            }catch(\Exception $e){
                Yii::$app->response->statusCode = 400;
                $transaction->rollBack();
                $response = [
                    'errors' =>  $errors
                ];
            }
        }else{
            Yii::$app->response->statusCode = 400;
            //workId not exists.
            $response = [
                'status' => false
            ];
        }

        return $response;
    }


}