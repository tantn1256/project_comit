$(document).ready(function () {
    //play audio
    $('#confirm-audio').on('click',function () {
        $('#audio-file')[0].play();
    });
    $('#btn-accept').on('click',function () {
        $('#modal-confirm').modal('show');
    });
    $('#btn-decline').on('click',function () {
        $('#modal-decline').modal('show');
    });

    //tu choi m31
    $('#btn-not-confirm').on('click',function () {
        $('#modal-confirm').modal('hide');
    });
    //tu choi m30
    $('#btn-no').on('click',function () {
        $('#modal-decline').modal('hide');
    });
    //Confirm work
    $('#btn-confirm').on('click',function () {
        var data = {
            'officeComment' : $('#confirm-comment').val(),
            'isConfirm' : 1,
            'csrf' : csrf
        };
        $.ajax({
            url: urlUpdateStatusWork,
            type: 'post',
            data: {
               data:data
            },
            success: function (response) {
                $('#modal-confirm').modal('hide');
                if(response == true){

                    $('#btn-accept').attr('disabled',true);
                    $('#btn-decline').attr('disabled',true);
                }else{
                    console.log('Error');
                }

            }
        });
    });
    //Decline work
    $('#btn-yes').on('click',function () {
        var data = {
            'officeComment' : $('#confirm-comment').val(),
            'isConfirm' : 1,
            'csrf' : csrf
        };
        $.ajax({
            url: urlUpdateStatusWork,
            type: 'post',
            data: {
               data:data
            },
            success: function (response) {
                $('#modal-decline').modal('hide');
                if(response == true){
                    $('#btn-accept').attr('disabled',true);
                    $('#btn-decline').attr('disabled',true);
                }else{
                    console.log('Error');
                }

            }
        });
    });

    //full images
    $('.img-responsive').on('click',function () {
        var tan = $(this).attr('src');
        $('.image_modal').attr('src',tan);
        $('#modal-image').modal('show');
        $('#modal-image').on('click',function () {
            $('#modal-image').modal('hide');
        });
    });

    //an button
    if(!isConfirmWorkDetail){
        $('#btn-decline').css('visibility','hidden');
        $('#btn-accept').css('visibility','hidden');
    }
});

