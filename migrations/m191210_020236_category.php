<?php

use yii\db\Migration;

/**
 * Class m191210_020236_category
 */
class m191210_020236_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191210_020236_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191210_020236_category cannot be reverted.\n";

        return false;
    }
    */
}
