<?php

use yii\db\Migration;

/**
 * Class m191231_085048_self_check_task
 */
class m191231_085048_self_check_task extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%self_check_task}}', [
            'id' => $this->primaryKey(),
            'userCode' => $this->string(45)->notNull(),
            'workId' => $this->bigInteger()->notNull(),
            'workTypeId' => $this->integer()->notNull(),
            'state' => $this->smallInteger()->notNull()->defaultValue(0),
            'createdAt' => $this->timestamp(),
            'updatedAt' => $this->timestamp(),
            'isAutomatic' => $this->smallInteger()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%self_check_task}}');
    }
}
