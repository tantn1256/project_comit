<?php

use yii\db\Migration;

/**
 * Class m200102_094602_alert_recommend
 */
class m200102_094602_alert_recommend extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%alert_recommend}}', [
            'id' => $this->primaryKey(),
            'workTypeId' => $this->integer()->notNull(),
            'organizationId' => $this->integer()->notNull(),
            'flowType' => $this->integer()->notNull(),
            'recommend' => $this->string(300)->notNull(),
            'createdAt' => $this->timestamp(),
            'updatedAt' => $this->timestamp(),
            'required' => $this->smallInteger(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%alert_recommend}}');
    }
}
