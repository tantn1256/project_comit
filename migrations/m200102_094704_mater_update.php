<?php

use yii\db\Migration;

/**
 * Class m200102_094704_mater_update
 */
class m200102_094704_mater_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%master_update}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string(100)->notNull(),
            'updateTime' => $this->timestamp()
        ]);
        $this->insert('master_update',['key' => 'recommend']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%master_update}}');
    }
}
