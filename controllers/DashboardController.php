<?php


namespace app\controllers;


use app\components\MyHelpers;
use app\models\Organization;
use app\models\User;
use app\models\Work;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;

class DashboardController extends Controller
{
    public $layout = 'master-login';
    public $array = [];
    /**
     * @param $token
     * @return string
     */


    public function actionIndex($organizationId){
        $organization = Organization::findOne(['id' => $organizationId]);
        $categories = Organization::find()->select('id,organizationName,parentId')->asArray()->all();
        $html_data = MyHelpers::showHtmlListData($organization,$categories);

        return $this->render('index',['data' => $html_data]);
    }

    public function showDashBoard($organizations, $parentId, $status)
    {
        $child = false;
        $query = (new Query())->SELECT([
            'w1.faOperationKind',
            'w1.start',
            'w1.end',
            'w1.createdAt',
            'w1.workedOrganizationId',
            'w1.status',
            'w1.id',
            'users.userName',
            'work_type.name nameWorkType',
            'up.*'
        ])
            ->FROM('work w1')
            ->INNERJOIN( 'users', 'w1.userCode = users.userCode')
            ->INNERJOIN('organization_parent up','w1.userCode = up.userCode')
            ->LEFTJOIN('self_check_task self', 'w1.id = self.workId')
            ->LEFTJOIN('work_type', 'self.workTypeId = work_type.id')
            ->WHERE('w1.id = (SELECT w2.id 
                    FROM work w2 
                    WHERE w2.userCode = w1.userCode AND w2.date = CURRENT_DATE 
                    ORDER BY w2.updatedAt DESC 
                    LIMIT 1)');
        if($status) {
            $query->andWhere(['w1.status' => $status]);
        } else {
            $query->andWhere('w1.status != 1 ')->andWhere('w1.status != 6');
        }
        $query->orderBy(['w1.createdAt' => SORT_DESC]);
        $listUserWork = $query->all();

        foreach ($organizations as $item)
        {
            if ($item['parentId'] == $parentId) {
                foreach ($listUserWork as $key => $work) {
                    if ($work['workedOrganizationId'] == $item['id']) {
                        $child = true;
                        if($work['status'] == 3) {
                            $this->array['waitting'][] = $work;

                        } else if($work['status'] == 2 || $work['status'] == 4){
                            $this->array['working'][] = $work;

                        } else if($work['status'] == 6) {
                            $this->array['finish'][] = $work;

                        } else if($work['status'] == 5) {
                            $this->array['decline'][] = $work;

                        }
                    }
                }
                if(!$child) {
                    $this->showDashBoard($organizations, $item['id'],$status);
                }
            }
        }
        if(!$child) {
            foreach ($listUserWork as $key => $work) {
                if ($parentId == $work['workedOrganizationId']) {
                    if($work['status'] == 3) {
                        $this->array['waitting'][] = $work;

                    } else if($work['status'] == 2 || $work['status'] == 4){
                        $this->array['working'][] = $work;

                    } else if($work['status'] == 6) {
                        $this->array['finish'][] = $work;

                    } else if($work['status'] == 5) {
                        $this->array['decline'][] = $work;
                    }

                }
            }
        }

        return $this->array;
    }

    public function actionDashboard($organizationId){
        //register
        $organization = Organization::findOne($organizationId);
        $categories = Organization::find()->select('id,organizationName,parentId')->asArray()->all();

        if(Yii::$app->request->isAjax){
                $getCheckbox  = Yii::$app->request->POST();
                $array_status = array();
                if ($getCheckbox['data']['cb-working'] == '1'){
                    $array_status[] = 2;
                    $array_status[] = 4;
                }
                if ($getCheckbox['data']['cb-confirm'] == '1'){
                    $array_status[] = 3;
                }
                if ($getCheckbox['data']['cb-decline'] == '1'){
                    $array_status[] = 5;
                }
                if ($getCheckbox['data']['cb-finished'] == '1'){
                    $array_status[] = 6;
                }
                if(count($array_status) > 0 )
                {
                    $data = $this->showDashBoard($categories, $organization['id'], $array_status);
                    return $this->renderAjax('table', ['data' => $data,'organization' => $organization,'categories'=>$categories]);
                }
                else
                {
                    $data = $this->showDashBoard($categories, $organization['id'], null);
                    return $this->renderAjax('table', ['data' => $data,'organization' => $organization,'categories'=>$categories]);
                }

        }else{
            $data = $this->showDashBoard($categories, $organization['id'], null);
            return $this->render('dashboard',['data' => $data,'organization' => $organization,'categories'=>$categories]);
        }
    }
}