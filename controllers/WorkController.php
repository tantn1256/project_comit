<?php


namespace app\controllers;


use app\models\Audio;
use app\models\ConfirmType;
use app\models\FilterForm;
use app\models\Image;
use app\models\Log;
use app\models\OfficeCheckTask;
use app\models\Organization;
use app\models\SelfCheckTask;
use app\models\User;
use app\models\Work;
use app\models\WorkType;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\validators\ValidationAsset;
use yii\web\Controller;
use app\Constant\Constant;
use yii\web\Response;

class WorkController extends Controller
{
    public $layout = 'master-login';

    /**
     * @param $id
     * @return string
     */
    public function actionWorkDetail($id){
        $work = Work::findOne($id);
        if($work){
            $user = User::findByUserCode($work->userCode);
            $data = [
                'id' => $id,
                'userCode' => $work->userCode,
                'userName' => $user->userName,
                'phone' => $user->phone,
                'start' => $work->start,
                'end' => $work->end,
                'status' => $work->status,
                'officeComment' => $work->officeComment
            ];
            $data['checkType'] = $work->checkType;
            $office_task = OfficeCheckTask::findOne(['workId' => $id]);
            $self_task = SelfCheckTask::findOne(['workId' => $id]);
            $task = '';
            $data['images'] = [];
            if ($office_task) {
                $task = $office_task->id;
                $confirmTypeId = $office_task->confirmTypeId;
                $data['workContent'] = ConfirmType::findOne($confirmTypeId)->name;
            }
            if ($self_task) {
                $task = $self_task->id;
                $workTypeId = $self_task->workTypeId;
                $data['workContent'] = WorkType::findOne($workTypeId)->name;
            }
            if ($task) {
                $audio = Audio::findOne(['taskId' => $task]);
                $images = Image::findAll(['taskId' => $task]);
                if($audio){
                    $data['audio'] = $audio->fileName;
                }
                if (is_array($images)) {
                    foreach ($images as $image) {
                        $data['images'][] = $image->fileName;
                    }
                }
            }
            return $this->render('work-detail',['data' => $data]);
        }
    }


    /**
     * @param $id
     * @return bool
     */
    public function actionUpdateStatus($id){
        if(Yii::$app->request->isAjax){
            $data = Yii::$app->request->post('data','');
            $work = Work::findOne($id);
            $response = false;
            //update comment confirm
            if(isset($data['officeComment']) && $data['officeComment']){
                $work->officeComment = $data['officeComment'];
            }
            $status = (isset($data['isConfirm']) && $data['isConfirm'] === "1") ? 4 : 5;
            //update status
            $work->status = $status;
            if($work->save()){
                $response = true;
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        }
    }

    public function actionWorkList(){
        $model = new FilterForm();
        $organizationId = Yii::$app->user->identity->organizationId;
        $organizationLevel = $this->getOrganizationLevel($organizationId);
        $dataWorkList = $this->getWorkList($organizationId,$organizationLevel);
        foreach ($dataWorkList as $key => $item){
            $office_task = OfficeCheckTask::findOne(['workId' => $item['id']]);
            $self_task = SelfCheckTask::findOne(['workId' =>$item['id']]);
            $task = '';
            if ($office_task) {
                $task = $office_task->id;
            }
            if ($self_task) {
                $task = $self_task->id;
            }
            if ($task) {
                $images = Image::findAll(['taskId' => $task]);
                if (is_array($images)) {
                    foreach ($images as $image) {
                        $dataWorkList[$key]['images'][] = $image->fileName;
                    }
                }else{
                    $dataWorkList[$key]['images'][]='';
                }
            }
        }
        $workType = WorkType::find()->select(['name'])->asArray()->all();
        return $this->render('work-list',['dataWorkList' => $dataWorkList,'model'=>$model,'workType'=>$workType]);
    }

    private function getOrganizationLevel($organizationId){
        $organization = Organization::findOne($organizationId);
        $level = $organization->organizationLevelId;
        $key = '';
        switch ($level){
            case $level == 1:
                $key = 'level_id 1';
                break;
            case $level == 2:
                $key = 'level_id 2';
                break;
            case $level == 3:
                $key = 'level_id 3';
                break;
            case $level == 4:
                $key = 'level_id 4';
                break;
            case $level == 5:
                $key = 'level_id 5';
                break;
        }
        return $key;
    }
    public function getWorkList($organizationId, $organizationLevel)
    {
        $query = (new Query())->SELECT([
            'w1.faOperationKind',
            'w1.start',
            'w1.end',
            'w1.createdAt',
            'w1.workedOrganizationId',
            'w1.status',
            'w1.id id',
            'w1.date',
            'w1.officeComment',
            'users.userName',
            'work_type.name nameWorkType',
            'confirm_type.name nameConfirmType',
            'audio.fileName as audioName',
            'confirm_type.aerialWork as officeArialWork',
            'work_type.aerialWork as selfArialWork',
            'op.*'

        ])
            ->FROM('work w1')
            ->INNERJOIN('users', 'w1.userCode = users.userCode')
            ->INNERJOIN('organization_parent op', 'w1.userCode = op.userCode')
            ->LEFTJOIN('self_check_task self', 'w1.id = self.workId')
            ->LEFTJOIN('work_type', 'self.workTypeId = work_type.id')
            ->LEFTJOIN('office_check_task office', 'w1.id = office.workId')
            ->LEFTJOIN('confirm_type', 'office.confirmTypeId = confirm_type.id')
            ->leftJoin('audio','(audio.taskId = office.id and audio.checkType = w1.checkType) 
                        or (audio.taskId = self.id and audio.checkType = w1.checkType )')
            ->WHERE('w1.id = (SELECT w2.id 
                    FROM work w2 
                    WHERE w2.userCode = w1.userCode 
                    ORDER BY w2.updatedAt DESC 
                    LIMIT 1)')
            ->andWhere('w1.status != 1')
            ->andWhere('w1.status != 6')
            ->andWhere([$organizationLevel => $organizationId])
            ->orderBy(['w1.createdAt' => SORT_DESC]);
        return $query->all();
    }




}