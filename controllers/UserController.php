<?php


namespace app\controllers;


use app\models\ResetPasswordForm;
use yii\web\Controller;
use Yii;

class UserController extends  Controller
{
    public $layout = 'master-login';

    /**
     * @return array|string
     */
    public function actionResetPassword(){
        $model = new ResetPasswordForm;
        if($model->load(Yii::$app->request->post()) &&  $model->validate()){
            if($model->validateUser()){
                Yii::$app->session->setFlash('success','Success');
            }
        }
        return $this->render('reset-password',['model' =>$model]);
    }
}