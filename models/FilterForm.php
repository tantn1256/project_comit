<?php


namespace app\models;

use kartik\date\DatePicker;
use yii\base\Model;

/**
 * This is the model class for table "work_type".
 *
 * @property DatePicker $startDate
 * @property DatePicker $endDate
 * @property DatePicker $startFinishTime
 * @property DatePicker $endFinishTime
 * @property string $workId
 * @property int $workType
 * @property string $branch
 * @property string $office
 * @property string $fsc
 * @property int $facility
 * @property int $employee
 * @property int $workingEmployee
 * @property int $workContent
 * @property int $kyDivision
 * @property int $result
 * @property array $level_name_2
 */

class FilterForm extends Model
{
    public $startDate;
    public $endDate;
    public $startFinishTime;
    public $endFinishTime;
    public $projectId;
    public $workType;
    public $identifyPlace;
    public $branch;
    public $office;
    public $fsc;
    public $facility;
    public $employee;
    public $workingEmployee;
    public $workContent;
    public $kyDivision;
    public $result;
    public $level_name_2;



}