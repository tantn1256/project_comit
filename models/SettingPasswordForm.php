<?php


namespace app\models;

use Yii;
use yii\base\Model;

class SettingPasswordForm extends Model
{
    public $userCode;
    public $password;
    public $repassword;

    public function rules()
    {
        return [
            [['userCode','password'], 'required'],
            ['password', 'string', 'length' => [8, 16], 'message' => '8 文字以上16文字以下で入力してください。'],
            ['password',function() {
                $check = strpos($this->password, $this->userCode);
                if($check !== false) {
                    $this->addError('password', 'パスワードに社員コード を含まないでください。 ');
                }
            }],
            ['repassword','required','whenClient' => "function(attribute,value){
                        return $('#setting-pwd').val() != '';
                    }"
            ],
            [['repassword'], 'compare', 'compareAttribute' => 'password', 'whenClient' => "function(attribute,value){
                        var password = $('#setting-pwd').val();
                        var repassword = $('#setting-re-pwd').val();
                        return password != '' && repassword != '';
                    
                    }"
            ],
            ['password', 'validatePassword'],

        ];
    }

    public function validatePassword($attribute,$params){
        $pass = str_split($this->$attribute);
        $isNumber = $isLowerCase = $isUpperCase = $isSpecialCharacter = $hasUserCode = false;
        if(stripos($this->$attribute,$this->userCode)){
            $hasUserCode = true;
        }
        if (!$hasUserCode){
            $checkValidation = 0;
            foreach ($pass as $value){
                if(ctype_digit($value)){
                    $isNumber = true;
                }
                else if(ctype_upper($value)){
                    $isUpperCase = true;
                }
                else if(ctype_lower($value)){
                    $isLowerCase = true;
                }
                else{
                    $isSpecialCharacter = true;
                }
                $checkValidation = $isNumber + $isUpperCase + $isLowerCase + $isSpecialCharacter;
                if ($checkValidation >= 2){
                    break;
                }
            }
            if($checkValidation >= 2){
                Yii::$app->session->setFlash('success','Validate Success');
            }else{
                $this->addError('server-error',Yii::t('app','パスワードは、アルファベット、大文字または小文字など、2種類以上の文字で構成されます。 '));
            }
        }else{
            $this->addError('server-error',Yii::t('app','パスワードにユーザーコードを使用していません'));
        }
    }




}