<?php
namespace app\components;
// namespace app\components; // For Yii2 Basic (app folder won't actually exist)
use app\controllers\DashboardController;
use app\models\SelfCheckTask;
use Dashborapp\Constant\Constant;
use app\models\Organization;
use app\models\User;
use app\models\Work;
use yii\helpers\Url;
use yii\db\Query;
use yii;

class MyHelpers {

    public $data = [];
    public static function showHtmlListData($organization,$categories){
        $data = [];
        $html_data = '';
        $data['name'] = $organization['organizationName'];
        $data['level'] = $organization['organizationLevelId'];
        $data['id'] = $organization['id'];
        $data['numberOfUsers'] = $organization['numberOfUsers'];

        $data_Child = Organization::findAll(['parentId' => $organization['id']]);
        $totalUser = ($data['numberOfUsers'] > 0) ? $data['numberOfUsers'] : '';

        $Working_total = self::getWorkingTotal($categories,$data['id']);
        $Working_total = ($Working_total > 0) ? $Working_total : '';

        $total_wait = self::getWaitListTotal($categories,$data['id']);
        $total_wait = ($total_wait > 0) ? $total_wait : '';

        $color = '#6f42c1';
        $color = ($total_wait > 0) ? $color : '';

        $urlDashboard = Url::to(['dashboard/dashboard', 'organizationId' => $data['id']]) ;
        if($data_Child && count($data_Child) > 0)
        {
            //hắn có các thằng con
            $html_data .='<div class="item">
                <div class="item-value" style="background: '.$color.'">'.$total_wait.'</div>
                <div class="item-value">'.$Working_total.'</div>
                <div class="item-value">'.$totalUser.'</div>
                <div class="name">
                    <span> <a href="'.$urlDashboard.'">'.$data['name'].'</a></span>
                    
                </div>';
            $html_data .='<div class="child">';
            //lấy tưng thang con

            foreach($data_Child as $item){
                $html_data .= self::showHtmlListData($item,$categories);
            }

            $html_data .='</div>';
            $html_data .='</div>';
            return $html_data;
        }
        else
        {
            //không có thằng con, in ra html
            $html_data = '<div class="item">
                        <div class="item-value" style="background: '.$color.'">'.$total_wait.'</div>
                        <div class="item-value">'.$Working_total.'</div>
                        <div class="item-value">'.$totalUser.'</div>
                        <div class="name">
                            <span> <a href="'.$urlDashboard.'">'.$data['name'].'</a></span>
                        </div>
                    </div>';
            return $html_data;
        }
    }

    //sum working
    public static function getWorkingTotal($categories, $parent_id)
    {
        $tong = 0;
        $array_organization_con = array();
        $array_organization_con = self::getOrganizationChild($categories,$parent_id,$array_organization_con);

        $sql = 'SELECT w1.* FROM work w1 
        WHERE w1.id = 
                    (SELECT id FROM work w2  
                    WHERE w2.userCode = w1.userCode and w2.date = CURRENT_DATE 
                    ORDER BY w2.updatedAt DESC LIMIT 1)               
        AND (w1.status = 2 or w1.status = 4)
        GROUP BY w1.workedOrganizationId';

        $userWorks = Work::findBySql($sql)->all();

        foreach ($userWorks as $key => $work)
        {
            if($work['workedOrganizationId'] == $parent_id )
            {
                $tong++;
            }
            if ( count($array_organization_con) > 0  &&  in_array($work['workedOrganizationId'],$array_organization_con))
            {
                $tong++;
            }
        }
        return $tong;
    }

    //Wait for confirm
    public static function getWaitListTotal($categories, $parent_id)
    {
        $tong_wait = 0;
        $array_organization_con = array();
        $array_organization_con = self::getOrganizationChild($categories,$parent_id,$array_organization_con);
        $sql_wait = 'SELECT w1.* FROM work w1 
        WHERE w1.id = 
                    (SELECT id FROM work w2  
                    WHERE w2.userCode = w1.userCode and w2.date = CURRENT_DATE 
                    ORDER BY w2.updatedAt ASC LIMIT 1)               
        AND (w1.status = 3)
        GROUP BY w1.workedOrganizationId';

        $userWait = Work::findBySql($sql_wait)->all();
        foreach ($userWait as $key => $work)
        {
            if($work['workedOrganizationId'] == $parent_id )
            {
                $tong_wait++;
            }
            if ( count($array_organization_con) > 0  &&  in_array($work['workedOrganizationId'],$array_organization_con))
            {
                $tong_wait++;
            }
        }
        return $tong_wait;
    }

    public static function getOrganizationChild($categories, $parent_id , $array_con = array())
    {
        foreach ($categories as $key => $item)
        {
            if ($item['parentId'] == $parent_id)
            {
                $array_con[] = $item['id'];
                unset($categories[$key]);
                $array_con = self::getOrganizationChild($categories, $item['id'], $array_con);
            }
        }
        return $array_con;
    }


    //dashboard
    public static function showListDashBoardNew($categories, $organizationId)
    {
        //buoc 1: lay tat ca caC THANG CON cua NO
        $array_organization_con = array();
        $array_organization_con = self::getOrganizationChild($categories,$organizationId,$array_organization_con);
        //lay array id cua cac organizations
        $array_organization_select = array();
        if($array_organization_con && count($array_organization_con) > 0  )
        {
            $array_organization_select = $array_organization_con;
        }
        else {
            $array_organization_select = array($organizationId);
        }
        foreach ($array_organization_select as $item)
        {

            $sql = 'SELECT w1.* 
                    FROM work w1
                    WHERE w1.id = (SELECT w2.id 
                    FROM work w2 
                    WHERE w2.userCode = w1.userCode AND w2.date = CURRENT_DATE 
                    ORDER BY w2.updatedAt DESC 
                    LIMIT 1) 
                    AND (w1.status = 3 or w1.status = 2 or w1.status = 4) 
                    GROUP BY w1.workedOrganizationId';

            $userWork = Work::findBySql($sql)->all();
            foreach ($userWork as $key => $work) {
                if ( $item == $work['workedOrganizationId']) {
                    $user = User::findOne(['id' => $work['workedOrganizationId']]);
                    $color = '';
                    if($work['status'] == 3 ) {
                        $color = '#ce8483';
                    } else {
                        $color = '#00FFFF';
                    }
                    echo '<tr style="background-color:'.$color.'">';


                    self::getParent($categories,$item);
                    echo '<td>';
                    echo $user->userName;
                    echo '</td>';
                    echo '</tr>';

                }
            }
        }
    }

    /**
     * @param $categories
     * @param $parent_id
     * @param array $status
     * @param array $data
     * @return mixed
     */
    public static function showDashBoard($status = array())
    {

        $item_child = false;
        $query = (new Query())->SELECT(['w1.*','us.*','users.userName','work_type.name nameWorkType'])
            ->FROM('work w1')
            ->INNERJOIN('users','w1.userCode = users.userCode')
            ->INNERJOIN('user_parrent us','w1.userCode = us.userCode')
            ->LEFTJOIN('self_check_task self','w1.id = self.workId')
            ->LEFTJOIN('work_type','self.workTypeId = work_type.id')
            ->WHERE('w1.id = (SELECT w2.id 
                    FROM work w2 
                    WHERE w2.userCode = w1.userCode AND w2.date = CURRENT_DATE 
                    ORDER BY w2.updatedAt DESC 
                    LIMIT 1)');

         if($status && count($status) > 0)
         {
             $query->ANDWHERE(['w1.status' => $status]);
         }else{
             $query->ANDWHERE('w1.status = 3 or w1.status = 2 or w1.status = 4 or w1.status = 5');
         }
        $query->GROUPBY('w1.workedOrganizationId')
              ->orderBy(['w1.start' => SORT_DESC]);

        $userWork = $query->all();

        return $userWork;

      /*  foreach ($categories as $item)
        {
            if ($item['parentId'] == $parent_id) {
                foreach ($userWork as $key => $work) {
                    if ($item['id'] == $work['workedOrganizationId']) {
                        $item_child = true;
                        if($work['status'] == 3 ) {
                            $data['waiting'][] = $work;
                        }else if ($work['status'] == 2 || $work['status'] == 4){
                            $data['working'][] = $work;
                        }
                        else if($work['status'] == 5){
                            $data['decline'][] = $work;
                        }
                        else if($work['status'] == 6){
                            $data['finish'][] = $work;
                        }
                    }
                }
                if (!$item_child){
                    self::showDashBoard($categories, $item['id'],$status);
                }
            }
        }
        if (!$item_child){
            foreach ($userWork as $key => $work) {
                if ($parent_id == $work['workedOrganizationId']) {
                    if($work['status'] == 3 ) {
                        $data['waiting'][] = $work;
                    }else if ($work['status'] == 2 || $work['status'] == 4){
                        $data['working'][] = $work;
                    }
                    else if($work['status'] == 5){
                        $data['decline'][] = $work;
                    }
                    else if($work['status'] == 6){
                        $data['finish'][] = $work;
                    }
                }
            }
        }

        return $data;*/
    }



    public static function getParent($categories,$parent_id, &$html_inra) {
        foreach ($categories as $item) {
            if($item['id'] == $parent_id) {
                if ($item['parentId']){
                    self::getParent($categories, $item['parentId'],$html_inra);
                    $html_inra .= '<td>';
                    $html_inra .= $item['organizationName'];
                    $html_inra .= '</td>';
                }
            }
        }
    }















}